package com.latihan.widiarta.myrecyclerview;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class DetailPresident extends AppCompatActivity {

    private Context context;
    public static final String EXTRA_NAME = "Nama ";
    public static final String EXTRA_REMARKS = "Remarks";
    public static final String EXTRA_FOTO = "KOSONG";


    TextView tvDataReceived;
    private ArrayList<President> listPresident;
    private ArrayList<President> getListPresident() {
        return listPresident;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_president);
        ImageView imgPhoto;
        TextView tvName, tvRemarks;

        tvDataReceived = findViewById(R.id.tv_data_received);
        imgPhoto = findViewById(R.id.img_item_photo);
        tvName = findViewById(R.id.tv_item_name);
        tvRemarks = findViewById(R.id.tv_item_remarks);


        String name = getIntent().getStringExtra(EXTRA_NAME);
        String remarks = getIntent().getStringExtra(EXTRA_REMARKS);
        String foto = getIntent().getStringExtra(EXTRA_FOTO);


        tvName.setText(name);
        tvRemarks.setText(remarks + ",\n Alamat Foto " + foto);

        // Harusnya kode program ini bisa nampilkan foto, karena data nya bisa tampil, tapi ketika ditamilkan malah error

        //Glide.with(context)
        //        .load(foto)
        //        .apply(new RequestOptions().override(350, 550))
        //        .into(imgPhoto);

    }
}
